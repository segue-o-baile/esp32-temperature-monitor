
#include <WiFi.h> 
#include <ArduinoMqttClient.h> 
#include <ArduinoJson.h> 
#include <OneWire.h>            
#include <DallasTemperature.h>  

#include "secrets.h"
#include "pitches.h"

const char* device_name = "esp32d_1";  


const char *wifi_ssid =  secret_wifi_ssid;
const char *wifi_password =  secret_wifi_password;
WiFiClient wifiClient; 


MqttClient mqttClient(wifiClient);
const char broker[]    = secret_mqtt_ip;
int        port        = secret_mqtt_port;

bool mqtt_retain = true;
int mqtt_qos = 1;


char mqtt_device_availability_topic[100];

char mqtt_ds18b20t_sensorName[50];
char mqtt_ds18b20t_sensorUid[50];
char mqtt_ds18b20t_configure_topic[100];
char mqtt_ds18b20t_state_topic[100];

const int oneWireBus = 4;     

OneWire oneWire(oneWireBus);



const int buzzerPin = 27;
int melody[] = {
  NOTE_C4
};


int noteDurations[] = {
  1
};

const int ledPin = 23;

DallasTemperature sensors(&oneWire);
float temperature;


unsigned long previousMillis = 0;
unsigned long interval = 60000; 

unsigned long pulsePreviousMillis = 0;
unsigned long pulseInterval = 60000; 
float max_temperature_safe = 37;


void setup() {
  Serial.begin(115200);
  Serial.updateBaudRate(1200);
  Serial.println("[#] STARTING: "); 
  Serial.println(device_name);


  
  WiFi.mode(WIFI_STA);
  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE, INADDR_NONE); 
  WiFi.setHostname(device_name); 
  WiFi.onEvent(wifi_connected, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_CONNECTED); 
  WiFi.onEvent(wifi_received_address, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_GOT_IP); 
  WiFi.onEvent(wifi_disconnected, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_DISCONNECTED); 
  WiFi.begin(wifi_ssid, wifi_password);
  wait_for_wifi();

  sensors.begin();

  
  mqtt_device_availability_set_variables();
  mqtt_ds18b20t_set_variables();

  
  mqtt_connect();

  
  ds18b20_read();
  ds18b20_serial_print();
  ds18b20_mqtt_send();


  pinMode(ledPin, OUTPUT);
  activate_buzzer();
  Serial.println("[#] END SETUP");
}


void loop() {
  digitalWrite(ledPin, HIGH);
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= interval){
    ds18b20_read();
    ds18b20_serial_print();
    ds18b20_mqtt_send();

    if(temperature > max_temperature_safe){
      Serial.print("Temperatura maxima atingida");
      activate_buzzer();
    }
    previousMillis = currentMillis;
  }


  mqttClient.poll(); 
  if(currentMillis - pulsePreviousMillis >= pulseInterval){
    if(WiFi.status() == WL_CONNECTED && !mqttClient.connected()){
      Serial.println("[#] MQTT disconnected");
      mqtt_connect();
    }
    pulsePreviousMillis = currentMillis;
  }
}

void activate_buzzer(){
    int noteDuration = 1000 / noteDurations[0];
    tone(buzzerPin, melody[0], noteDuration);
    
    
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    
    noTone(buzzerPin);
}

void wifi_connected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.print("[+] Connected to AP: "); Serial.print(WiFi.SSID()); Serial.print(", "); Serial.println(WiFi.RSSI());
}
void wifi_received_address(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.print("[+] Received IP: ");
  Serial.print(WiFi.localIP());
  Serial.print(" | ");
  Serial.println(WiFi.getHostname());
}
void wifi_disconnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.print("[#] wifi disconnected: ");
  Serial.println(info.wifi_sta_disconnected.reason); 
  Serial.println("[#] Retrying");
  WiFi.reconnect();
  wait_for_wifi();
}
void wait_for_wifi(){
  Serial.println("[#] wifi connecting");
  unsigned long timeout_count = millis();
  while (WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
    if(millis() - timeout_count >= 15000){
      Serial.println("[#] Timed out, restarting");
      ESP.restart();
    }
  }
}


void mqtt_connect(){
  mqttClient.setId(device_name);
  
  
  mqttClient.beginWill(mqtt_device_availability_topic, mqtt_retain, mqtt_qos);
  mqttClient.print("offline");
  mqttClient.endWill();
  if (!mqttClient.connect(broker, port)) {
    Serial.print("[#] MQTT failed: ");
    Serial.println(mqttClient.connectError());
    
    delay(15000);
    Serial.println("[#] Timed out, restarting");
    ESP.restart();
  }
  Serial.println("[+] MQTT connected");
  mqtt_send_device_configure();
  mqtt_device_availability("online"); 
  mqttClient.onMessage(mqtt_on_message); 
  mqttClient.subscribe(mqtt_device_availability_topic, mqtt_qos);
}
void mqtt_on_message(int messageSize) {
  Serial.print("[+] MQTT RECEIVED: ");
  if(mqttClient.messageTopic() == mqtt_device_availability_topic){ 
    if(mqttClient.available()){
      int mqtt_length = messageSize + 1;
      byte mqtt_payload[mqtt_length] = {0};
      mqttClient.readBytes(mqtt_payload, mqtt_length);
      if(strcmp("offline", (char *)mqtt_payload) == 0){
        Serial.print("device status: "); Serial.println((char *)mqtt_payload);
        mqtt_device_availability("online");
        mqtt_send_device_configure();
        ds18b20_mqtt_send();
      } else {
        Serial.print("device status: "); Serial.println((char *)mqtt_payload);
      }
    }
  } else {
    Serial.print(mqttClient.messageTopic()); Serial.print(": ");
    while (mqttClient.available()) {
      Serial.print((char)mqttClient.read());
    }
    Serial.println();
  }
}
void mqtt_send_device_configure(){
  mqtt_ds18b20t_configure();
}
void mqtt_device_availability(char* send_data){
  mqttClient.beginMessage(mqtt_device_availability_topic, true, mqtt_qos);
  mqttClient.print(send_data);
  mqttClient.endMessage();
  Serial.print("[M] Sent availability: "); Serial.print(mqtt_device_availability_topic); Serial.print(":"); Serial.println(send_data);
}
void mqtt_device_availability_set_variables(){
  snprintf(mqtt_device_availability_topic, sizeof(mqtt_device_availability_topic), "esp32/%s/status", device_name);
}
void mqtt_ds18b20t_set_variables(){
  snprintf(mqtt_ds18b20t_sensorName, sizeof(mqtt_ds18b20t_sensorName), "%s_ds18b20t", device_name);
  snprintf(mqtt_ds18b20t_sensorUid, sizeof(mqtt_ds18b20t_sensorUid), "%s_uid", mqtt_ds18b20t_sensorName);
  snprintf(mqtt_ds18b20t_configure_topic, sizeof(mqtt_ds18b20t_configure_topic), "esp32/sensor/%s/temperature/config", device_name);
  snprintf(mqtt_ds18b20t_state_topic, sizeof(mqtt_ds18b20t_state_topic), "esp32/sensor/%s/temperature/state", device_name);
}

void mqtt_ds18b20t_configure(){
  DynamicJsonDocument doc(512);
  doc["name"] = mqtt_ds18b20t_sensorName;
  doc["uniq_id"] = mqtt_ds18b20t_sensorUid;
  doc["avty_t"] = mqtt_device_availability_topic;
  doc["pl_avail"] = "online";
  doc["pl_not_avail"] = "offline";
  doc["stat_t"] = mqtt_ds18b20t_state_topic; 
  doc["unit_of_meas"] = "°C";
  doc["dev_cla"] = "temperature";
  doc["val_tpl"] =  "{{value|float|default(0)|round(1)}}"; 
  JsonObject device = doc.createNestedObject("device");
  device["ids"][0] = device_name;
  device["mf"] = device_name;
  device["mdl"] = device_name;
  device["name"] = device_name;
  mqttClient.beginMessage(mqtt_ds18b20t_configure_topic, (unsigned long)measureJson(doc), mqtt_retain, mqtt_qos); 
  serializeJson(doc, mqttClient);
  mqttClient.endMessage();
}


void ds18b20_read(){
  Serial.println("[#] Reading ds18b20");
  sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);
  temperature = temperatureC;
}
void ds18b20_serial_print(){
    Serial.print("T:"); Serial.print(temperature); Serial.print("*C"); Serial.println(" | ");
}
void ds18b20_mqtt_send(){
  mqttClient.beginMessage(mqtt_ds18b20t_state_topic, false, mqtt_qos);
  mqttClient.print(temperature);
  mqttClient.endMessage();
}
